# empezamos con **ip a**

En linux es habitual utilizar algún comando que nos de información sobre nuestras direcciones ip.

Un dispositivo que es capaz de comunicarse a través de una red en linux se denomina una **interface** de red. Así por ejemplo una tarjeta ethernet, una tarjeta wifi o un modem para red móvil 4G son diferentes tipos de interface de red.

El comando **ifconfig**, que vendría a ser una abreviatura de "interface configuration", es el comando que históricamente se ha usado en linux desde las primeras distribuciones.

Una salida típica del comando ifconfig:


```
$ ifconfig
enp2s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.19  netmask 255.255.0.0  broadcast 192.168.255.255
        inet6 fe80::96de:80ff:fe49:7dfd  prefixlen 64  scopeid 0x20<link>
        ether 94:de:80:49:7d:fd  txqueuelen 1000  (Ethernet)
        RX packets 134404  bytes 123322816 (117.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 86085  bytes 9558809 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1  (Local Loopback)
        RX packets 3404  bytes 1184015 (1.1 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3404  bytes 1184015 (1.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


```

Posteriormente se dieron cuenta que ese comando era muy limitado y se necesitaban de otros comandos para configurar otros aspectos de la red. Posteriormente apareción el comando **ip** que unificó el uso de varios comandos y amplió la cantidad de operaciones que se podían haer. Permite configurar aspectos avanzados de las interfaces y las rutas de red, y actualmente viene por defecto instalado en la mayor parte de distribuciones de linux.

Al comando ip, cuando se le pasa como segundo parámetro **address** sirve para manejar todo lo que tenga que ver con las direcciones ip de las interfaces de red.

El comando ***ip address show*** ofrece información sobre las direcciones de las interfaces de red. Este comando se puede abreviar y este listado de órdenes en realidad es la misma orden:
```
ip address show
ip address s
ip addr s
ip a s
ip a

```



## Entender la salida del comanda "ip a"

Una salida típica del comando 'ip a' es:

```
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:fd brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.19/16 brd 192.168.255.255 scope global dynamic enp2s0
       valid_lft 8354sec preferred_lft 8354sec
    inet6 fe80::96de:80ff:fe49:7dfd/64 scope link
       valid_lft forever preferred_lft forever

```

La salida se divide en conjuntos de varias líneas por cada interface. Las interfaces que encuentra en el sistema las numera correlativamente.

La primera interfaz identificada como **lo** es una interfaz especial, se trata de una interface de red que permite comunicarse al sistema operativo con el mismo a través de una red. Se conoce como interface de loopback, que hace referencia a que cuando envías unos datos a través de esa interfaz vuelve a ti mismo.

Interesa más empezar analizando una la segunda interface, que es una tarjeta de red ethernet que es la que en este equipo permite la salida a internet a través de un cable de red que lleva conectado.

A partir de un análiis de la primera línea deducimios mucha información

```
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
|  |        |         |         |  |         |                     
A  B        C         D         E  F         G                     

```
- A.  **numeración de la interface**, no es un número importante, sólo sirve para identificar cuantas interfaces hay en el sistema y para ayudar a que la salida del comando sea más legible

- B.  **nombre de la interface de red**. Por defecto cada distribución de linux denomina a las interfaces de red siguiendo una convención de nombres. En este caso se usó Fedora y sigue un esquema de nombres que explican en  [su manual](https://docs.fedoraproject.org/en-US/Fedora/24/html/Networking_Guide/ch-Consistent_Network_Device_Naming.html). Históricamente el esquema de nombres era menos consistente, pero con nombres más sencillos, y aun se sigue usando en otras distribuciones. ethX para

- C.  BROADCAST: la interfaz puede enviar tramas de  [broadcast](https://es.wikipedia.org/wiki/Difusi%C3%B3n_amplia). Esta opción es habitual que aparezca siempre activa, excepto en la interface de loopback.

- D.  MULTICAST: la interfaz puede enviar tramas [multicast](https://es.wikipedia.org/wiki/Multidifusi%C3%B3n). Esta opción es habitual que aparezca siempre activa, excepto en la interface de loopback.

- E.  **UP: interfaz habilitada**. La interface está lista para detectar señal y operar. Para realizar ciertas operaciones en la interface, como por ejemplo cambiar el nombre, hay que deshabilitar la interface. Cuando la interface está deshabilitada la palabra UP no aparece

- F.  **LOWER UP: hay señal**. La interface puede emitir y recibir. En el caso de las conexiones cableadas ethernet significa que detecta conexión con el switch, y es habitual que las tarjetas de red informen con un led encendido. En el caso de las conexiones wifi considera que hay señal cuando ha sido capaz de asociarse a un Access Point.

- G:  **mtu 1500**: maximum transfer unit = tamaño máximo de cada trama de datos, como máximo se pueden enviar 1500 bytes por trama y la información que hay que enviar a través de la tarjeta de red
se ha de romper en fragmentos de 1500 bytes, que es el tamaño de trama más habitual en las redes ethernet

Sobre el resto de información que aporta esta línea no se profundizará en este momento.

Las siguientes líneas informan sobre direcciones de diferentes protocolos


### Dirección ethernet

```
    link/ether 94:de:80:49:7d:fd brd ff:ff:ff:ff:ff:ff
    |          |
    A          B
```

Las comunicaciones cableadas ([IEEE 802.3](https://en.wikipedia.org/wiki/IEEE_802.3) / ethernet) e inalámbricas ([IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11) / WiFi ) se caracterizan por enviar los datos separándolos en tramas. Para poder identificar el origen y destino de esa trama dentro de una misma red estos protocolos usan direcciones MAC. La dirección MAC está pensada para que sea única y en principio cada interface de red que se contruya tiene una MAC distinta. De esta manera no es necesario configurar estas direcciones, ya que están asociadas a la propia tarjeta de red y es responsabilida del fabricante que cada dispositivo de red que quiera comunicarse usando estos protocolos tenga una dirección MAC única.

Las direcciones MAC están compuestas por 6 bytes. Cada byte se representa con dos números hexadecimales y se separa con dos puntos del siguiente byte para facilitar su lectura. 

Los 3 primeros bytes identifican al fabricantes de la tarjeta de red, y los 3 últimos bytes es responsabilidad de ese fabricante que no se repitan. Por ejemplo dos pcs del mismo modelo tendrán los 3 primeros bytes iguales y los 3 últimos bytes distintos. Los 3 primeros bytes se denominan OUI (Organizationally Unique Identifier). Se pueden consultar los códigos asignados a los fabricantes en una web del [IEEE](http://standards-oui.ieee.org/oui/oui.txt) 



### Dirección IPv4

El protocolo IP (Internet Protocol) sirve para comunicar dos equipos interconectados a través de diferentes redes. Este protocolo identifica los equipos con una dirección IP. 

```
inet 192.168.0.19/16 brd 192.168.255.255 scope global dynamic enp2s0
|    |            |                                   |
A    B            C                                   D
       valid_lft 8354sec preferred_lft 8354sec
       |
       E
```


A.  inet es la abreviatura de internet. La información que aparece a continuación hace referencia por tanto a la dirección de internet, es decir, a la dirección ip.

B.  Dirección IP asociada a esa interfaz. Consiste en 4 numeros de 0 a 255 separados por puntos. 256 números son las combinaciones posibles que se pueden hacer con 8 bits. Es decir, se dispone sólo de 4 bytes para definir una dirección IP, lo que limita el número de direcciones IPs posibles a 256x256x256x256 = 4294967296 que es un número insuficiente de IPs para la cantidad mundial de dispositivos conectados a internet actualmente.

Esta limitación obliga a tener que compartir una ip pública entre los dispositivos de una red privada, realizando la conversión a la ip pública cuando se sale a internet. Para eso se han definido unos rangos de ips privadas reservados para usos internos. Estos rangos son:

* 	10.0.0.0 - 10.255.255.255
*   172.16.0.0 - 172.31.255.255
*   192.168.0.0 - 192.168.255.255

C. La máscara de subred. Indica el número de bits que forman parte de la dirección de red. Sirve para decidir que ips forman parte de mi subred y que ips necesitan de un router para comunicarse con ellas. 

D. Indica que la ip es dinámica, si la ip es estática esta palabra no aparece

E. Cuando la ip es dinámica se nos asigna una ip desde un servidor DHCP. Esa dirección IP no es para siempre, y ha de ser renovada. En este caso indican que la ip es válida durante los siguientes 8354 segundos. Si la ip es estática valid_lft tiene el valor forever (nunca caduca la dirección ip).


## Dirección IPv6

El rango de direcciones IPv4 se quedó muy limitado para el número de dispositivos conectados a internet que se necesitaba intercomunicar. Para suplir esta limitación en la dimensión de las direcciones se creó el protocolo IPv6 que permite trabajar con 8 números hexadecimales de 4 dígitos. Una dirección IPv6 se codifica por tanto con 128 bits. En nuestro caso la interfaz enp2s0 dispone de una dirección ipv6 que aparece con el prefijo inet6:

```
    inet6 fe80::96de:80ff:fe49:7dfd/64 scope link
       valid_lft forever preferred_lft forever
       
```    

Las direcciones IPv6 son difíciles de recordar y muy largas de escribir, para facilitar un poco su lectura se ha establecido una notación que comprime un poco su longitud cuando hay ceros. Todas estas notaciones de una misma dirección están permitidas:

```
2001:0DB8:0000:0000:0000:0000:1428:57ab
2001:0DB8:0000:0000:0000::1428:57ab
2001:0DB8:0:0:0:0:1428:57ab
2001:0DB8:0::0:1428:57ab
2001:0DB8::1428:57ab
```
